package com.somitsolutions.android.gpxtopbf;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

//import com.nm.android.gpxtopbf.R;
import com.somitsolutions.android.gpxtopbf.GPXInXMLRepresentation.Rte;
import com.somitsolutions.android.gpxtopbf.GPXInXMLRepresentation.Trk;
import com.somitsolutions.android.gpxtopbf.GPXInXMLRepresentation.TrkSeg;
import com.somitsolutions.android.gpxtopbf.GPXInXMLRepresentation.Wpt;
import com.somitsolutions.android.gpxtopbf.GPXToPBFProtos.GpxProto;
import com.somitsolutions.android.gpxtopbf.GPXToPBFProtos.RteProto;
import com.somitsolutions.android.gpxtopbf.GPXToPBFProtos.TrkProto;
import com.somitsolutions.android.gpxtopbf.GPXToPBFProtos.TrkSegProto;
import com.somitsolutions.android.gpxtopbf.GPXToPBFProtos.WptProto;

public class GPXToPBFActivity extends Activity {
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
       String path = Environment.getExternalStorageDirectory().toString();
		
		String tempFilePath = path + "/Temp/fells_loop.pbf";
		
		String debugFilePath = path + "/Temp/debugtxt.txt";
		
		//This is for debug
		FileWriter fStream = null;
		//BufferedWriter outBuf = null;
		try {
			fStream = new FileWriter(debugFilePath);
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		
		GPXInXMLRepresentation gpxInXMLRepresentation = null;
		
		//GPXParserHandler gpxParser = new GPXParserHandler();
		
		try {
			gpxInXMLRepresentation = ParseFromGPX.parse(getAssets().open("fells_loop.gpx"));
        } 
    catch (IOException e) {
    	
    	Log.d("XML","onCreate(): parse() failed");
        return;
    }
		
		OutputStream oS = null;
		try {
			oS = new FileOutputStream(tempFilePath, true);
		} catch (FileNotFoundException e1){
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		try {
			//GpxProto.build().writeTo(oS);
			GpxProto tempGpxProto = GPXParserHandler.GpxProto.build();
			tempGpxProto.writeTo(oS);
			BufferedWriter outBuf = new BufferedWriter(fStream);
			String strDebug = tempGpxProto.toString();
			outBuf.write(strDebug);
			
			oS.close();
			outBuf.close();
		} catch (IOException e){
			e.printStackTrace();
		}
		
			
    }
 }