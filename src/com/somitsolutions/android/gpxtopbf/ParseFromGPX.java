package com.somitsolutions.android.gpxtopbf;

import java.io.InputStream;

import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import android.util.Log;

public class ParseFromGPX {
	
	public static int mWptCount = 0;
	public static int mRtePtCount = 0;
	public static int mTrkPtCount = 0;

	public static GPXInXMLRepresentation parse(InputStream is) {
		GPXInXMLRepresentation mGPXInXMLRepresentation = null;
		try {
			// create a XMLReader from SAXParser
			XMLReader xmlReader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
			
			GPXParserHandler gpxHandler = new GPXParserHandler();
			
			xmlReader.setContentHandler(gpxHandler);
			
			xmlReader.parse(new InputSource(is));
			
			mWptCount = GPXParserHandler.mWptCount;
			
			mRtePtCount = GPXParserHandler.mRtePtCount;
			
			mTrkPtCount = GPXParserHandler.mTrkPtCount;
			
			mGPXInXMLRepresentation = gpxHandler.getGPXInXMLRepresentation();

		} catch(Exception ex) {
			Log.d("XML", "StudyParser: parse() failed");
		}
		return mGPXInXMLRepresentation;
	}
}
