package com.somitsolutions.android.gpxtopbf;

import java.util.ArrayList;

public class GPXInXMLRepresentation {
	
	public ArrayList<Wpt> mWptArray = new ArrayList<GPXInXMLRepresentation.Wpt>();
	public Gpx mGpx = new Gpx();
	
	 public class Wpt {
		 
			double mEle;
			int mTime;

			public String mName;
			public double mLat;
			int mLat_diff;
			double mLon;
			int mLon_diff;
		}
	 
	 public class Rte {
			public String name;
			public ArrayList<Wpt> rtePt = new ArrayList<GPXInXMLRepresentation.Wpt>();
		}
	 
	public class TrkSeg {
		public ArrayList<Wpt> trkpt = new ArrayList<GPXInXMLRepresentation.Wpt>();
	}
	 
	 public class Trk {
		 public String name;
		 public ArrayList<TrkSeg> trkseg = new ArrayList<GPXInXMLRepresentation.TrkSeg>();
	 }
	 
	 public class Pt {
		double ele;
		int time;
		double lat;
		double lon; 
	 }
	 
	 public class PtSeg {
		 ArrayList<Pt> pt = new ArrayList<GPXInXMLRepresentation.Pt>();
	 }
	 
	 public class Bounds {
		 double minlat;
		 double minlon;
		 double maxlat;
		 double maxlon;
			
	 }
	 
	public class Metadata {
		 String name;
		 String description;
	 }
	
	 public class Gpx {
		 public Metadata metadata;
		 public ArrayList<Wpt> wpt = new ArrayList<GPXInXMLRepresentation.Wpt>();
		 public ArrayList<Rte> rte = new ArrayList<GPXInXMLRepresentation.Rte>();
		 public ArrayList<Trk> trk = new ArrayList<GPXInXMLRepresentation.Trk>();	
		}
	 
	 public class GpxParseRequest {
		 Byte gpx; 
	 }
	 
	 public class GpxParseResponse {
		 Gpx gpx;
		 String err;
	 }	 
}
