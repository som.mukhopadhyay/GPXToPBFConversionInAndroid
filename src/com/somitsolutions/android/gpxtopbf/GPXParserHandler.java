package com.somitsolutions.android.gpxtopbf;


import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.somitsolutions.android.gpxtopbf.GPXInXMLRepresentation.Wpt;
import com.somitsolutions.android.gpxtopbf.GPXToPBFProtos.GpxProto;
import com.somitsolutions.android.gpxtopbf.GPXToPBFProtos.RteProto;
import com.somitsolutions.android.gpxtopbf.GPXToPBFProtos.TrkProto;
import com.somitsolutions.android.gpxtopbf.GPXToPBFProtos.TrkSegProto;
import com.somitsolutions.android.gpxtopbf.GPXToPBFProtos.WptProto;

public class GPXParserHandler extends DefaultHandler {
	
	public static int mWptCount = 0;
	public static int mRteCount = 0;
	public static int mTrkCount = 0;
	public static int mRtePtCount = 0;
	public static int mTrkPtCount = 0;
	
	private GPXInXMLRepresentation mGPXInXMLRepresentation;
	private GPXInXMLRepresentation.Wpt mWpt;
	private GPXInXMLRepresentation.Gpx mGpx;
	
	private GPXInXMLRepresentation.Wpt mRteWpt;
	private GPXInXMLRepresentation.Rte mRte;
	private GPXInXMLRepresentation.TrkSeg mTrkSeg;
	private GPXInXMLRepresentation.Trk mTrk;
	private GPXInXMLRepresentation.Wpt mTrkSegWpt;
	
	
	////+++ Test
	
	public WptProto.Builder wptProto = WptProto.newBuilder();
	
	public RteProto.Builder rteProto = RteProto.newBuilder();
	
	public WptProto.Builder rtePtProto =  WptProto.newBuilder();
	
	public TrkProto.Builder trkProto = TrkProto.newBuilder();
	
	public TrkSegProto.Builder trkSegProto = TrkSegProto.newBuilder();
	
	public WptProto.Builder trkSegPtProto =  WptProto.newBuilder();
	
	public static GpxProto.Builder GpxProto = com.somitsolutions.android.gpxtopbf.GPXToPBFProtos.GpxProto.newBuilder();
	
	
	/////+++Test
	
	public GPXInXMLRepresentation getGPXInXMLRepresentation(){
		return mGPXInXMLRepresentation;
	}
	@Override
	public void startDocument() throws SAXException {
		// create new object
		mGPXInXMLRepresentation = new GPXInXMLRepresentation();
		mGpx = mGPXInXMLRepresentation.new Gpx();
		//mWpt = mGPXInXMLRepresentation.new Wpt();
		
	}
	
	@Override
	public void endDocument() throws SAXException {
		// nothing we need to do here
	}
	
	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts)
							throws SAXException {
		
		//WPT
		if(localName.equals("wpt")) {
			
			//wptProto = WptProto.newBuilder();
			
			// Get the number of attribute
	        int length = atts.getLength();
	        
			//String tempLatString = new String("wpt@lat");
			//String tempLonString = new String("wpt@lon");
			mWpt = mGPXInXMLRepresentation.new Wpt();
			
			// Process each attribute
	        for (int i=0; i<length; i++) {
	        	String attrName = atts.getQName(i);
	        	if (attrName.equalsIgnoreCase("LAT")){
	        		mWpt.mLat = Double.parseDouble(atts.getValue(i));
	        	}
	        	
	        	if (attrName.equalsIgnoreCase("LON")){
	        		mWpt.mLon = Double.parseDouble(atts.getValue(i));
	        	}	
	        }
	        GpxProto.addWpt(WptProto.newBuilder().setLat(mWpt.mLat)
	        		.setLon(mWpt.mLon).build());
	        //mGpx.wpt.add(mWpt);
			//mGPXInXMLRepresentation.mWptArray.add(mWpt);
			mWptCount++;
		}
		
		/*//+++ RTE
		if(localName.equals("rte")) {
			GpxProto.addRte(RteProto.newBuilder().
		}*/
			
			if(localName.equals("rtept")){				
				// Get the number of attribute
	        int length = atts.getLength();
	        
			//String tempLatString = new String("wpt@lat");
			//String tempLonString = new String("wpt@lon");
	        mRte = mGPXInXMLRepresentation.new Rte();
	        mRteWpt = mGPXInXMLRepresentation.new Wpt();
			
			// Process each attribute
	        for (int i=0; i<length; i++) {
	        	String attrName = atts.getQName(i);
	        	if (attrName.equalsIgnoreCase("LAT")){
	        		mRteWpt.mLat = Double.parseDouble(atts.getValue(i));
	        	}
	        	
	        	if (attrName.equalsIgnoreCase("LON")){
	        		mRteWpt.mLon = Double.parseDouble(atts.getValue(i));
	        	}	
	        }
	        
	        //mRte.rtePt.add(mRteWpt);
	       // mGpx.rte.add(mRte);
	        rteProto.addRtept(WptProto.newBuilder().setLat(mRteWpt.mLat).setLon(mRteWpt.mLon).build());
	        mRtePtCount++;
			}
			
			if(localName.equals("rte")) {
				GpxProto.addRte(rteProto.build());
		//}
			}
		
		//++++TRK
		if(localName.equals("trkpt")) {
			
			int length = atts.getLength();
			mTrkSeg = mGPXInXMLRepresentation.new TrkSeg();
			mTrkSegWpt = mGPXInXMLRepresentation.new Wpt();
			
			 for (int i=0; i<length; i++) {
		        	String attrName = atts.getQName(i);
		        	if (attrName.equalsIgnoreCase("LAT")){
		        		mRteWpt.mLat = Double.parseDouble(atts.getValue(i));
		        	}
		        	
		        	if (attrName.equalsIgnoreCase("LON")){
		        		mRteWpt.mLon = Double.parseDouble(atts.getValue(i));
		        	}	
		        }
			//mTrkSeg.trkpt.add(mTrkSegWpt);
			 //mTrk.trkseg.add(mTrkSeg);
			 //mGpx.trk.add(mTrk);
			 trkSegProto.addTrkpt(WptProto.newBuilder().setLat(mRteWpt.mLat).setLon(mRteWpt.mLon).build());
			 mTrkPtCount++;
		}
		
		///+++
			
		if(localName.equals("trkseg")) {
			trkProto.addTrkseg(trkSegProto.build());
		}
		
		if (localName.equals("trk")){
			GpxProto.addTrk(trkProto.build());
		}
	}
	
	 
		@Override
		public void endElement(String namespaceURI, String localName, String qName) throws SAXException {

			if(localName.equals("wpt")) {
			}
			
			else if(localName.equals("wpt")) {
				
			}
		}
		
		@Override
		public void characters(char ch[], int start, int length) {
			String chars = new String(ch, start, length);
		    chars = chars.trim();
		    
		}
}
